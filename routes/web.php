<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');


Route::prefix('riwayat-saldo')->group(function () {
    Route::get('/', 'App\Http\Controllers\RiwayatSaldoController@list')->name('riwayat-saldo');
});

Route::resource('transaksi', 'App\Http\Controllers\TransaksiController')->parameters([
    'transaksi' => 'transaksi_id'
])->middleware(['auth']);

Route::resource('saldo', 'App\Http\Controllers\SaldoController')->parameters([
    'saldo' => 'saldo_id'
])->middleware(['auth']);


require __DIR__.'/auth.php';


