<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Riwayat Saldo
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div id='recipients' class="p-8 mt-6 lg:mt-0 rounded shadow bg-white">
                        <h3>Saldo: {{ $saldo }}</h3>
                        <hr>
                    </div>
                    <!--Card-->
                    <div id='recipients' class="p-8 mt-6 lg:mt-0 rounded shadow bg-white">
                        <table id="example" class="stripe hover" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
                            <thead>
                            <tr>
                                <th data-priority="1">Nama Transaksi</th>
                                <th data-priority="2">Type</th>
                                <th data-priority="3">Nominal Transaksi</th>
                                <th data-priority="3">Saldo Sebelumnya</th>
                                <th data-priority="4">Tgl Transaksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($data as $item)
                            <tr>
                                @foreach ($item->transaksi()->get() as $trx)
                                <td>{{ $trx->trx_name }}</td>
                                <td>{{ $trx->trx_type }}</td>
                                <td>{{ $trx->trx_amount }}</td>
                                <td>{{ $item->current_balance }}</td>
                                <td>{{ $trx->trx_date }}</td>
                                @endforeach
                            </tr>
                            @endforeach
                            <!-- Rest of your data (refer to https://datatables.net/examples/server_side/ for server side processing)-->
                            </tbody>

                        </table>

                    </div>
                    <!--/Card-->
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
