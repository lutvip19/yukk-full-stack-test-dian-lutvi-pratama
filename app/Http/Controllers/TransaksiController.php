<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaksi;
use App\Models\RiwayatSaldo;
use App\Models\Saldo;
use Illuminate\Support\Facades\Validator;
use Auth;

class TransaksiController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('transaksi');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('transaksi');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        // validate incoming request
        $validator = Validator::make($request->all(), [
            'trx_title' => 'required',
            'trx_type' => 'required',
            'trx_amount' => 'required|numeric',
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            //Session::flash('error', $validator->messages()->first());
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }
        else {
            $user_id = Auth::id();
            $saldo = Saldo::where('user_id', $user_id)->first()->amount ?? 0;
            //dd($saldo);

            $trx_amount = $request->trx_amount ?? 0;
            $trx_type = $request->trx_type;
            $saldo_amount = $trx_type == "in" ? ($saldo + $trx_amount) : ($saldo - $trx_amount);
            //dd($saldo_amount);

            $transaksi = new Transaksi();
            $transaksi->user_id = $user_id;
            $transaksi->trx_name = $request->trx_title;
            $transaksi->trx_type = $request->trx_type;
            $transaksi->trx_amount = $request->trx_amount;
            $transaksi->trx_date = date('Y-m-d H:i:s');
            $transaksi->description = $request->description;
            $transaksi->save();

            $trx_id = $transaksi->id;
            $r_saldo = new RiwayatSaldo();
            $r_saldo->user_id = $user_id;
            $r_saldo->trx_id = $trx_id;
            $r_saldo->current_balance = $saldo;
            $r_saldo->save();

            Saldo::where('user_id', $user_id)
                ->update(['amount' => $saldo_amount]);
        }

        return redirect()->route('transaksi.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
