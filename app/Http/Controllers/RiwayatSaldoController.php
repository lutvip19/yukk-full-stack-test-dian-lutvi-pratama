<?php

namespace App\Http\Controllers;

use App\Models\Saldo;
use Illuminate\Http\Request;
use App\Models\RiwayatSaldo;
use Auth;

class RiwayatSaldoController extends Controller
{
    private $perpage = 10;
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function list()
    {
        //
        $user_id = Auth::id();
        $saldo = Saldo::where('user_id', $user_id)->first()->amount ?? 0;

        $data = RiwayatSaldo::all();

        //dump($data->first());
        //dd($data->first()->transaksi()->get());


        return view('riwayat-saldo',[
            'data' => $data,
            'saldo' => $saldo,
        ]);
    }
}
