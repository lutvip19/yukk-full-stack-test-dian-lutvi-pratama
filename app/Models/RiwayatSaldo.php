<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RiwayatSaldo extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'riwayat_saldo';

    public function transaksi()
    {
        return $this->hasOne('App\Models\Transaksi', 'id', 'trx_id');
    }
}
